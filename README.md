# cstream-sse-provider  

## About
Provides data from cstream backend to SSE.

## Run  
To run you need:  
- running Kafka on port given in yaml properties file  
- use specific profile for instance: `-Dspring.profiles.active=dev`