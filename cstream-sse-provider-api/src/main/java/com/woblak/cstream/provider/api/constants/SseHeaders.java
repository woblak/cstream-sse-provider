package com.woblak.cstream.provider.api.constants;

public abstract class SseHeaders {

    public static final String KEY = "key";
    public static final String EVENT = "event";
}
