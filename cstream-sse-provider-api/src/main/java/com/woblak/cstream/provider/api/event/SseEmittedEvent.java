package com.woblak.cstream.provider.api.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SseEmittedEvent {

    private AggregationType type;
    private String code;
    private String windowName;

    @RequiredArgsConstructor
    @Getter
    public enum AggregationType {
        SUMMARY("summary"),
        MEAN("mean");

        private final String type;
    }
}
