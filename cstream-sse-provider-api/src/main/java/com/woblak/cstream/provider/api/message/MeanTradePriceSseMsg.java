package com.woblak.cstream.provider.api.message;

public class MeanTradePriceSseMsg implements SseMsg {

    public static final String EVENT = "meanPrice-data";

    private MeanTradePriceSseMsg() {

    }
}
