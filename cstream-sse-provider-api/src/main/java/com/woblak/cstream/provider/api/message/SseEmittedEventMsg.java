package com.woblak.cstream.provider.api.message;

import com.woblak.cstream.provider.api.event.SseEmittedEvent;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

@RequiredArgsConstructor
@Getter
@Builder
public class SseEmittedEventMsg implements Message<SseEmittedEvent> {

    public static final String EVENT = "sseEmitted-event";
    public static final String VERSION = "1.0";

    private final MessageHeaders headers;
    private final SseEmittedEvent payload;
}
