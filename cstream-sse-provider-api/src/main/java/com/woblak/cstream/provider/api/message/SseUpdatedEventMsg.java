package com.woblak.cstream.provider.api.message;

import com.woblak.cstream.provider.api.event.SseEmittedEvent;
import com.woblak.cstream.provider.api.event.SseUpdatedEvent;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

@RequiredArgsConstructor
@Getter
@Builder
public class SseUpdatedEventMsg implements Message<SseUpdatedEvent> {

    public static final String EVENT = "sseUpdated-event";
    public static final String VERSION = "1.0";

    private final MessageHeaders headers;
    private final SseUpdatedEvent payload;
}
