package com.woblak.cstream.provider.api.message;

public class SummaryTradeSseMsg implements SseMsg {

    public static final String EVENT = "summary-data";

    private SummaryTradeSseMsg() {

    }
}
