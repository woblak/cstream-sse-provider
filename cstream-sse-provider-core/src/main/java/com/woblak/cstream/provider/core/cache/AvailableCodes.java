package com.woblak.cstream.provider.core.cache;

import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class AvailableCodes {

    private final Set<String> codes = new HashSet<>();

    public void add(String code) {
        codes.add(code);
    }

    public Set<String> getCodes() {
        return new HashSet<>(codes);
    }
}
