package com.woblak.cstream.provider.core.cache;

import com.woblak.cstream.provider.core.util.Tuple;
import lombok.RequiredArgsConstructor;

import java.time.Clock;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.ToLongFunction;
import java.util.stream.Collectors;

/*
/*
Example v:
{
  code: BTCUSD,
  windowDurationName: PT1H10M,
  price: 61333
}

Example cache:
{
    BTCUSD:{
            PT1H10M: {
                    code: BTCUSD,
                    windowDurationName: PT1H10M,
                    price: 61333
            },
            PT1H5M: {
                    code: BTCUSD,
                    windowDurationName: PT1H5M,
                    price: 61332
            }
    },
    DOTUSD:{
            PT1H10M: {
                    code: DOTUSD,
                    windowDurationName: PT1M10S,
                    price: 1333
            },
            PT1H5M: {
                    code: DOTUSD,
                    windowDurationName: PT1H5M,
                    price: 1332
            }
    }
}
 */
@RequiredArgsConstructor
public class ClosedWindowCache<K1, K2, V> implements WindowCache<K1, K2, V> {

    private final ConcurrentMap<K1, ConcurrentMap<K2, Values<V>>> cache = new ConcurrentHashMap<>();
    private final ToLongFunction<V> timestampExtractor;
    private final Clock clock;

    private static final long RETENTION_MS = 1000 * 60 * 60;

    @Override
    public boolean updateCache(K1 k1, K2 k2, V v) {
        ConcurrentMap<K2, Values<V>> cachedWindows = cache.getOrDefault(k1, new ConcurrentHashMap<>());
        Values<V> cachedValues = cachedWindows.computeIfAbsent(k2, key -> new Values<>(timestampExtractor));
        cachedValues.add(v);
        cachedWindows.put(k2, cachedValues);
        cache.put(k1, cachedWindows);
        return true;
    }

    @Override
    public Map<K2, V> getCachedValue(K1 k) {
        long currentTimestamp = clock.millis();
        return cache.get(k).entrySet().stream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey(),
                        entry -> entry.getValue().getValue(currentTimestamp),
                        (a, b) -> b));
    }

    @Override
    public List<Tuple<K1, Map<K2, V>>> getAllCachedValues() {
        return cache.keySet().stream()
                .map(key -> Tuple.of(key, getCachedValue(key)))
                .collect(Collectors.toList());
    }

    private static class Values<V> {

        private final ToLongFunction<V> timestampExtractor;
        private SortedByTimestampLinkedList<V> values;

        public Values(ToLongFunction<V> timestampExtractor) {
            this.timestampExtractor = timestampExtractor;
            this.values = new SortedByTimestampLinkedList<>(timestampExtractor);
        }

        synchronized V getValue(long currentTimestamp) {
            V result;
            SortedByTimestampLinkedList<V> oldValues = values.getFiltered(value -> timestampExtractor.applyAsLong(value) <= currentTimestamp);
            if (oldValues.isEmpty()) {
                result = values.isEmpty() ? null : values.get(0);
            } else {
                result = oldValues.get(oldValues.size() - 1);
                this.values = values.getFiltered(value -> timestampExtractor.applyAsLong(value) >= currentTimestamp - RETENTION_MS);
            }
            return result;
        }

        synchronized boolean add(V v) {
            return values.add(v);
        }
    }
}
