package com.woblak.cstream.provider.core.cache;

import com.woblak.cstream.provider.core.util.Tuple;
import lombok.RequiredArgsConstructor;

import java.time.Clock;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.ToLongFunction;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class OpenedWindowCache<K1, K2, V> implements WindowCache<K1, K2, V> {

    private final ConcurrentMap<K1, ConcurrentMap<K2, OpenedWindowCache.Values<V>>> cache = new ConcurrentHashMap<>();
    private final ToLongFunction<V> timestampExtractor;
    private final Clock clock;

    @Override
    public boolean updateCache(K1 k1, K2 k2, V v) {
        ConcurrentMap<K2, OpenedWindowCache.Values<V>> cachedWindows = cache.getOrDefault(k1, new ConcurrentHashMap<>());
        OpenedWindowCache.Values<V> cachedValues = cachedWindows.computeIfAbsent(k2, key -> new Values<>(timestampExtractor));
        cachedValues.add(v);
        cachedWindows.put(k2, cachedValues);
        cache.put(k1, cachedWindows);
        return true;
    }

    @Override
    public Map<K2, V> getCachedValue(K1 k) {
        long currentTimestamp = clock.millis();
        return cache.get(k).entrySet().stream()
                .collect(Collectors.toMap(key -> key.getKey(),
                        v -> v.getValue().getValue(currentTimestamp),
                        (a, b) -> b));
    }

    @Override
    public List<Tuple<K1, Map<K2, V>>> getAllCachedValues() {
        return cache.keySet().stream()
                .map(key -> Tuple.of(key, getCachedValue(key)))
                .collect(Collectors.toList());
    }

    private static class Values<V> {

        private ToLongFunction<V> timestampExtractor;
        private SortedByTimestampLinkedList<V> values;

        public Values(ToLongFunction<V> timestampExtractor) {
            this.timestampExtractor = timestampExtractor;
            this.values = new SortedByTimestampLinkedList<>(timestampExtractor);
        }

        synchronized V getValue(long currentTimestamp) {
            V result;
            SortedByTimestampLinkedList<V> newValues = values.getFiltered(value -> timestampExtractor.applyAsLong(value) >= currentTimestamp);
            if (newValues.isEmpty()) {
                result = values.get(values.size() - 1);
            } else {
                this.values = newValues;
                result = newValues.get(0);
            }
            return result;
        }

        synchronized boolean add(V v) {
            return values.add(v);
        }
    }
}
