package com.woblak.cstream.provider.core.cache;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.ToLongFunction;
import java.util.stream.Collectors;


public class SortedByTimestampLinkedList<E> {

    private final List<E> list;
    private final ToLongFunction<E> timestampExtractor;

    public SortedByTimestampLinkedList(ToLongFunction<E> timestampExtractor) {
        this(timestampExtractor, new LinkedList<>());
    }

    public SortedByTimestampLinkedList(ToLongFunction<E> timestampExtractor, List<E> list) {
        this.timestampExtractor = timestampExtractor;
        this.list = list;
    }

    public E get(int i) {
        return list.get(i);
    }

    public SortedByTimestampLinkedList<E> getFiltered(Predicate<E> predicate) {
        List<E> filteredList = list.stream()
                .filter(predicate)
                .collect(Collectors.toList());
        return new SortedByTimestampLinkedList<>(timestampExtractor, filteredList);
    }

    public boolean add(E e) {
        if (list.isEmpty()) {
            list.add(e);
        }
        long vTimestamp = timestampExtractor.applyAsLong(e);
        for (int i = 0; i < list.size(); i++) {
            E idxValue = list.get(i);
            long idxTimestamp = timestampExtractor.applyAsLong(idxValue);
            if (vTimestamp < idxTimestamp) {
                list.add(i, e);
                break;
            } else if (vTimestamp == idxTimestamp) {
                list.set(i, e);
                break;
            } else if (i + 1 == list.size()) {
                list.add(e);
                break;
            }
        }
        return true;
    }

    public int size() {
        return list.size();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }
}
