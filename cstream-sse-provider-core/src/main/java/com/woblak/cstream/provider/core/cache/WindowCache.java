package com.woblak.cstream.provider.core.cache;

import com.woblak.cstream.provider.core.util.Tuple;

import java.util.List;
import java.util.Map;

public interface WindowCache<K1, K2, V> {

    boolean updateCache(K1 k1, K2 k2, V v);

    Map<K2, V> getCachedValue(K1 k1);

    List<Tuple<K1,Map<K2, V>>> getAllCachedValues();
}
