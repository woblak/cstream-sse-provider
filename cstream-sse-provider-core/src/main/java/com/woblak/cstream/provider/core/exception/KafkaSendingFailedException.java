package com.woblak.cstream.provider.core.exception;

public class KafkaSendingFailedException extends RuntimeException {

    public KafkaSendingFailedException(Throwable cause) {
        super(cause);
    }
}
