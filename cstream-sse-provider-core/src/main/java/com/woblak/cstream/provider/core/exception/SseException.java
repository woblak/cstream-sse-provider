package com.woblak.cstream.provider.core.exception;


public class SseException extends RuntimeException {

    public SseException(String message, Exception e) {
        super(message, e);
    }
}