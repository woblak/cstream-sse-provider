package com.woblak.cstream.provider.core.io.kafka.config;

import com.woblak.cstream.api.KafkaTopics;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;

@Profile({"metrics", "prod", "dev"})
@Configuration
@Slf4j
public class KafkaTopicConfig {

    private final String bootstrapAddress;
    private final int sseNotificationsTopicPartitions;
    private final int sseNotificationsTopicReplicas;

    public KafkaTopicConfig(
            @Value("${spring.kafka.bootstrap-servers}") String bootstrapAddress,
            @Value("${spring.kafka.topics.sse-notifications.partitions:1}") int sseNotificationsTopicPartitions,
            @Value("${spring.kafka.topics.sse-notifications.replicas:1}") int sseNotificationsTopicReplicas
    ) {
        this.bootstrapAddress = bootstrapAddress;
        this.sseNotificationsTopicPartitions = sseNotificationsTopicPartitions;
        this.sseNotificationsTopicReplicas = sseNotificationsTopicReplicas;
    }

    @Bean
    public NewTopic sseNotificationsTopic() {
        return TopicBuilder.name(KafkaTopics.SSE_NOTIFICATIONS)
                .partitions(sseNotificationsTopicPartitions)
                .replicas(sseNotificationsTopicReplicas)
                .build();
    }

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = topicConfigs();
        return new KafkaAdmin(configs);
    }

    @Bean
    public Map<String, Object> topicConfigs() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        return configs;
    }

    @Bean
    public Map<String, Object> topicConfigs(
            @Value("${spring.kafka.bootstrap-servers}") String bootstrapAddress
    ) {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        return configs;
    }
}