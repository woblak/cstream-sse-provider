package com.woblak.cstream.provider.core.io.kafka.consumer;

import com.woblak.cstream.aggregator.api.event.MeanTradePrice;
import com.woblak.cstream.aggregator.api.event.SummaryTrade;
import com.woblak.cstream.api.KafkaHeaders;
import com.woblak.cstream.api.KafkaTopics;
import com.woblak.cstream.provider.core.config.AppProps;
import com.woblak.cstream.provider.core.io.kafka.handler.MsgHandler;
import com.woblak.cstream.provider.core.util.CommonUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.KafkaListenerErrorHandler;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.StringJoiner;

@Profile({"metrics", "prod", "dev"})
@KafkaListener(
        topics = {KafkaTopics.CRYPTO_AGG_SUMMARY, KafkaTopics.CRYPTO_AGG_MEAN},
        containerFactory = "kafkaListenerContainerFactory",
        errorHandler = "validationErrorHandler")
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class CryptoAggListener {

    private final Set<MsgHandler<SummaryTrade>> summaryHandlers;
    private final Set<MsgHandler<MeanTradePrice>> meanPriceHandlers;
    private final AppProps appProps;

    @KafkaHandler
    public void listen(@Payload SummaryTrade payload, @Headers MessageHeaders headers) {
        try {
            handle(payload, headers, summaryHandlers);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @KafkaHandler
    public void listen(@Payload MeanTradePrice payload, @Headers MessageHeaders headers) {
        try {
           handle(payload, headers, meanPriceHandlers);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @KafkaHandler(isDefault = true)
    public void listenDefault(@Payload Object payload, @Headers MessageHeaders headers) {
        log.info(createLogMsg("Ignored unrecognized", payload, headers));
    }

    @Bean
    public KafkaListenerErrorHandler validationErrorHandler() {
        return (m, e) -> {
            StringJoiner sj = new StringJoiner("\n")
                    .add(createLogMsg("Rejected", m.getPayload(), m.getHeaders()))
                    .add(e.getMostSpecificCause().getMessage());
            log.error(sj.toString());
            return null;
        };
    }

    private String createLogMsg(String part, Object payload, MessageHeaders headers) {
        String payloadToLog = CommonUtils.truncateString(payload.toString(), appProps.getPayloadMaxLength());
        return String.format("%s msg with event: [%s] version: [%s] headers: [%s] and payload: [%s]",
                part, headers.get(KafkaHeaders.EVENT), headers.get(KafkaHeaders.VERSION), headers, payloadToLog);
    }

    private <T> void handle(T payload, MessageHeaders headers, Set<MsgHandler<T>> handlers) {
        handlers.stream()
                .filter(h -> h.test(headers))
                .forEach(h -> {
                    h.accept(payload, headers);
                });
    }
}
