package com.woblak.cstream.provider.core.io.kafka.handler;

import com.woblak.cstream.aggregator.api.event.MeanTradePrice;
import com.woblak.cstream.provider.core.cache.AvailableCodes;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AvailableCodeMsgHandler implements MsgHandler<MeanTradePrice> {

    private final AvailableCodes availableCodes;

    @Override
    public boolean test(MessageHeaders headers) {
        return true;
    }

    @Override
    public void accept(MeanTradePrice payload, MessageHeaders headers) {
        availableCodes.add(payload.getCode());
    }
}
