package com.woblak.cstream.provider.core.io.kafka.handler;

import com.woblak.cstream.aggregator.api.event.MeanTradePrice;
import com.woblak.cstream.api.KafkaHeaders;
import com.woblak.cstream.api.KafkaTopics;
import com.woblak.cstream.provider.api.constants.SseHeaders;
import com.woblak.cstream.provider.api.event.SseUpdatedEvent;
import com.woblak.cstream.provider.api.message.MeanTradePriceSseMsg;
import com.woblak.cstream.provider.api.message.SseUpdatedEventMsg;
import com.woblak.cstream.provider.core.cache.OpenedWindowCache;
import com.woblak.cstream.provider.core.cache.WindowCache;
import com.woblak.cstream.provider.core.io.kafka.producer.KafkaSender;
import com.woblak.cstream.provider.core.io.sse.emitter.CryptoSseEmitter;
import com.woblak.cstream.provider.core.util.Tuple;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@Slf4j
public class MeanPriceMsgHandler implements MsgHandler<MeanTradePrice> {

    private final CryptoSseEmitter cryptoSseEmitter;
    private final KafkaSender kafkaSender;
    private final WindowCache<String, String, MeanTradePrice> windowCache;
    private final ScheduledExecutorService emittingExecutor;
    private final int notifyOnEveryCount;

    private final AtomicInteger counter = new AtomicInteger();

    @Autowired
    public MeanPriceMsgHandler(
            CryptoSseEmitter cryptoSseEmitter,
            KafkaSender kafkaSender,
            Clock clock,
            @Value("${emitter.initDelayMs:5000}") long initDelayMs,
            @Value("${emitter.delayMs:1000}") long delayMs,
            @Value("${emitter.notifyOnEveryCount:40}") int notifyOnEveryCount
    ) {
        this.cryptoSseEmitter = cryptoSseEmitter;
        this.kafkaSender = kafkaSender;
        this.windowCache = new OpenedWindowCache<>(p -> p.getMetaData().getEndMs(), clock);
        this.emittingExecutor = Executors.newSingleThreadScheduledExecutor();
        this.emittingExecutor.scheduleWithFixedDelay(
                () -> {
                    try {
                        emittAll();
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                    }
                },
                initDelayMs, delayMs, TimeUnit.MILLISECONDS
        );
        this.notifyOnEveryCount = notifyOnEveryCount;
    }

    @Override
    public boolean test(MessageHeaders headers) {
        return true;
    }

    @Override
    public void accept(MeanTradePrice payload, MessageHeaders headers) {
        windowCache.updateCache(payload.getCode(), payload.getWindowDurationName(), payload);
        if(counter.addAndGet(1) % notifyOnEveryCount == 0)
        notifyKafka(payload.getCode(),
                headers.getOrDefault(KafkaHeaders.RECEIVED_TIMESTAMP, "-1").toString());
    }

    private void emittAll() {
        windowCache.getAllCachedValues()
                .stream()
                .filter(tuple -> tuple.first != null)
                .filter(tuple -> tuple.second != null)
                .forEach(this::emitt);
    }

    private void emitt(Tuple<String, Map<String, MeanTradePrice>> tuple) {
        Message<Map<String, MeanTradePrice>> msg = MessageBuilder.withPayload(tuple.second)
                .setHeader(SseHeaders.KEY, tuple.first)
                .setHeader(SseHeaders.EVENT, MeanTradePriceSseMsg.EVENT)
                .build();
        cryptoSseEmitter.convertAndSend(msg);
    }


    private void notifyKafka(String code, String receivedTimestamp) {
        var event = SseUpdatedEvent.builder()
                .code(code)
                .type(SseUpdatedEvent.AggregationType.MEAN)
                .build();
        Message<SseUpdatedEvent> msg = MessageBuilder.withPayload(event)
                .setHeader(KafkaHeaders.TOPIC, KafkaTopics.SSE_NOTIFICATIONS)
                .setHeader(KafkaHeaders.EVENT, SseUpdatedEventMsg.EVENT)
                .setHeader(KafkaHeaders.VERSION, SseUpdatedEventMsg.VERSION)
                .setHeader(KafkaHeaders.LAST_TIMESTAMP, receivedTimestamp)
                .build();
        kafkaSender.send(msg);
    }
}
