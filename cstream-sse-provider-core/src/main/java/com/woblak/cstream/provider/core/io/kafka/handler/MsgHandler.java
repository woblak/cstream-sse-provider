package com.woblak.cstream.provider.core.io.kafka.handler;

import org.springframework.messaging.MessageHeaders;

public interface MsgHandler<T> {

    boolean test(MessageHeaders headers);

    void accept(T payload, MessageHeaders headers);
}
