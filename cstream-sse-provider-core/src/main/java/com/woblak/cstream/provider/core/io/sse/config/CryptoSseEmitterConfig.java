package com.woblak.cstream.provider.core.io.sse.config;

import com.woblak.cstream.provider.core.io.sse.emitter.CryptoSseEmitter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CryptoSseEmitterConfig {

    @Bean
    public CryptoSseEmitter aggSseEmitter(@Value("${server.port}") String serverPort) {
        return new CryptoSseEmitter(serverPort, "agg");
    }
}
