package com.woblak.cstream.provider.core.io.sse.config;

import com.woblak.cstream.provider.core.io.sse.emitter.CryptoSseEmitter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
@EnableScheduling
public class HeartBeatScheduler {

    private final Set<CryptoSseEmitter> emitters;

    @Scheduled(cron = "${sse.heartBeat.cron:*/30 * * * * *}")
    public void heartBeat() {
        emitters.forEach(CryptoSseEmitter::heartBeat);
    }
}
