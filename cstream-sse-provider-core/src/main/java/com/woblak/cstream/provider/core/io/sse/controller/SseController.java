package com.woblak.cstream.provider.core.io.sse.controller;

import com.woblak.cstream.provider.core.cache.AvailableCodes;
import com.woblak.cstream.provider.core.io.sse.emitter.CryptoSseEmitter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/events")
@Slf4j
public class SseController {

    private final String serverPort;
    private final CryptoSseEmitter aggSseEmitter;
    private final AvailableCodes availableCodes;

    @Autowired
    public SseController(
            @Value("${server.port}") String serverPort,
            CryptoSseEmitter aggSseEmitter,
            AvailableCodes availableCodes
    ) {
        this.serverPort = serverPort;
        this.aggSseEmitter = aggSseEmitter;
        this.availableCodes = availableCodes;
    }

    @GetMapping(path = "/agg/{codes}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter getAggEmitter(@PathVariable String codes) {
        log.info("Requested connection to sse for codes [{}]", codes);
        return aggSseEmitter.registerNewSseEmitter(codes);
    }

    @GetMapping(path = "/codes")
    public List<String> codes() {
        return new ArrayList<>(availableCodes.getCodes());
    }

    @GetMapping("/test")
    public String test() {
        var sb = new StringBuilder();
        return sb.append("Current server.port: ")
                .append(serverPort)
                .append("<br>")
                .append("Current SSE connections number: ")
                .append(aggSseEmitter.getCurrEmmitersNumber())
                .toString();
    }
}
