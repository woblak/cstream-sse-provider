package com.woblak.cstream.provider.core.io.sse.emitter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.woblak.cstream.provider.api.constants.SseHeaders;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.serializer.support.SerializationFailedException;
import org.springframework.messaging.Message;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
@EqualsAndHashCode
public class CryptoSseEmitter implements Emitter {

    private static final String HEART_BEAT = "heartBeat";

    private final String serverPort;
    private final String name;

    private final CopyOnWriteArrayList<SseEmitterWithCodes> sseEmitters;

    private AtomicInteger msgId;
    private AtomicInteger heartBeatId;

    private ObjectMapper om;

    public CryptoSseEmitter(String serverPort, String name) {
        this.serverPort = serverPort;
        this.name = name;
        this.sseEmitters = new CopyOnWriteArrayList<>();
        this.msgId = new AtomicInteger();
        this.heartBeatId = new AtomicInteger();
        this.om = new ObjectMapper();
    }

    public SseEmitter registerNewSseEmitter(String codes) {
        List<String> codeList = Arrays.asList(codes.split(","));
        return createSseEmitter(codeList);
    }

    public SseEmitter registerNewSseEmitter(List<String> codeList) {
        return createSseEmitter(codeList);
    }

    private SseEmitter createSseEmitter(List<String> codeList) {
        var sseEmitter = new SseEmitter((long) (1000 * 300));
        var sseEmitterWithCodes = SseEmitterWithCodes.builder()
                .codeList(codeList)
                .sseEmitter(sseEmitter)
                .build();

        sseEmitter.onError(e -> {
            log.error(e.getMessage(), e);
            sseEmitter.complete();
        });

        sseEmitter.onCompletion(() -> {
            log.info("completion sseEmitter {}", name);
            sseEmitter.complete();
            sseEmitters.remove(sseEmitterWithCodes);
        });

        sseEmitters.add(sseEmitterWithCodes);
        return sseEmitter;
    }

    @Override
    public void convertAndSend(Message<?> msg) {
        String code = msg.getHeaders().get(SseHeaders.KEY).toString();
        String event = msg.getHeaders().get(SseHeaders.EVENT).toString();
        Object data = msg.getPayload();
        convertAndSend(code, event, data);
    }

    @Override
    public void convertAndSend(String code, String event, Object data) {
        String json = convertToJson(data);
        convertAndSend(code, event, json);
    }

    public void convertAndSend(String code, String event, String data) {
        SseEmitter.SseEventBuilder msg = SseEmitter.event()
                .name(event)
                .id(String.valueOf(msgId.incrementAndGet()))
                .data(data);
        send(msg, code);
    }

    @Override
    public void heartBeat() {
        SseEmitter.SseEventBuilder msg = SseEmitter.event()
                .name(HEART_BEAT)
                .id(String.valueOf(heartBeatId.incrementAndGet()))
                .data(String.valueOf(sseEmitters.size()) + serverPort);
        send(msg);
    }

    public String getName() {
        return name;
    }

    public int getCurrEmmitersNumber() {
        return sseEmitters.size();
    }

    private String convertToJson(Object data) {
        String json;
        try {
            json = om.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
            throw new SerializationFailedException(e.getMessage(), e);
        }
        return json;
    }

    private void send(SseEmitter.SseEventBuilder event) {
        List<SseEmitter> allSseEmitters = sseEmitters.stream()
                .map(SseEmitterWithCodes::getSseEmitter)
                .collect(Collectors.toList());
        send(event, allSseEmitters);
    }

    private void send(SseEmitter.SseEventBuilder event, String code) {
        List<SseEmitter> filteredSseEmitters = sseEmitters.stream()
                .filter(emitter -> emitter.getCodeList().contains(code))
                .map(SseEmitterWithCodes::getSseEmitter)
                .collect(Collectors.toList());
        send(event, filteredSseEmitters);
    }

    private void send(SseEmitter.SseEventBuilder event, List<SseEmitter> sseEmitters) {
        sseEmitters.forEach(sseEmitter -> {
            try {
                sseEmitter.send(event);
            } catch (Exception e) {
                log.info(e.getMessage());
            }
        });
        logSentEvent(event);
    }

    private void logSentEvent(SseEmitter.SseEventBuilder event) {
        List<ResponseBodyEmitter.DataWithMediaType> list = new ArrayList<>(event.build());
        String metaMsg = list.get(0).getData().toString().replaceAll("\\n", " ");
        String dataMsg = list.get(1).getData().toString();
        log.info(">> sse >> topic: [{}] event:[{}]", name, metaMsg + dataMsg);
    }
}
