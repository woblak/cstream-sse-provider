package com.woblak.cstream.provider.core.io.sse.emitter;

import org.springframework.messaging.Message;

public interface Emitter {

    void convertAndSend(Message<?> msg);

    void convertAndSend(String code, String event, Object data);

    void convertAndSend(String code, String event, String data);

    void heartBeat();
}
