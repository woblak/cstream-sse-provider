package com.woblak.cstream.provider.core.io.sse.emitter;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Builder
@EqualsAndHashCode
public class SseEmitterWithCodes {

    @Builder.Default
    private final List<String> codeList = new ArrayList<>();

    private final SseEmitter sseEmitter;
}
