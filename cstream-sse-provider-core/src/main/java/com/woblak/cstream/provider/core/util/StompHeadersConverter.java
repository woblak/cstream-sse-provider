package com.woblak.cstream.provider.core.util;

import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.simp.stomp.StompHeaders;

public interface StompHeadersConverter {

    MessageHeaders toMessageHeaders(StompHeaders stompHeaders);

    StompHeaders toStompHeaders(MessageHeaders messageHeaders);
}