package com.woblak.cstream.provider.core.util;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Tuple<T1, T2> {

    public final T1 first;
    public final T2 second;

    public static <T1, T2> Tuple<T1, T2> of (T1 first, T2 second) {
        return new Tuple<>(first, second);
    }
}
