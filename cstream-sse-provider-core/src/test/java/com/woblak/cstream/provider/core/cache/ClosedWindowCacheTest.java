package com.woblak.cstream.provider.core.cache;

import lombok.Value;
import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.util.Map;

import static org.junit.Assert.assertEquals;

class ClosedWindowCacheTest {

    Clock clock = Clock.systemUTC();

    @Test
    void shouldTakeValueWithTheBiggestTimestampBeforeCurrentTimestamp() {
        // given
        String key = "a";
        TestObject to3 = new TestObject(3, 3);
        TestObject to1 = new TestObject(1, 1);
        TestObject to2 = new TestObject(2, 2);
        TestObject to4 = new TestObject(91636205001152L, 4);

        // when
        WindowCache<String, String, TestObject> windowCache = new ClosedWindowCache<>(TestObject::getTimestmap, clock);
        windowCache.updateCache(key, key, to3);
        windowCache.updateCache(key, key, to1);
        windowCache.updateCache(key, key, to2);
        Map<String, TestObject> result = windowCache.getCachedValue("a");

        // then
        assertEquals(Map.of(key, to3), result);
    }

    @Test
    void shouldTakeValueWithTheSmallestTimestampAfterCurrentTimestamp() {
        // given
        String key = "a";
        TestObject to2 = new TestObject(91636205001152L, 2);
        TestObject to3 = new TestObject(91636205001151L, 3);
        TestObject to1 = new TestObject(91636205001153L, 1);


        // when
        WindowCache<String, String, TestObject> windowCache = new ClosedWindowCache<>(TestObject::getTimestmap, clock);
        windowCache.updateCache(key, key, to3);
        windowCache.updateCache(key, key, to1);
        windowCache.updateCache(key, key, to2);
        Map<String, TestObject> result = windowCache.getCachedValue("a");

        // then
        assertEquals(Map.of(key, to3), result);
    }


    @Value
    static class TestObject {
        long timestmap;
        int value;
    }
}
