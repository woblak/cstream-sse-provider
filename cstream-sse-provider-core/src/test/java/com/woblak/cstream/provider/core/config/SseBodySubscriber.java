package com.woblak.cstream.provider.core.config;

import lombok.extern.slf4j.Slf4j;

import java.net.http.HttpResponse;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Flow;
import java.util.function.Consumer;

@Slf4j
public class SseBodySubscriber implements HttpResponse.BodySubscriber<Void> {

    private final Consumer<? super String> messageDataConsumer;
    private final CompletableFuture<Void> future;
    private Flow.Subscription subscription;

    public SseBodySubscriber(Consumer<? super String> messageDataConsumer) {
        this.messageDataConsumer = messageDataConsumer;
        this.future = new CompletableFuture<>();
    }

    @Override
    public CompletionStage<Void> getBody() {
        return future;
    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        subscription.request(1);
        this.messageDataConsumer.accept("");
    }

    @Override
    public void onNext(List<ByteBuffer> buffers) {
        try {
            for (var buffer : buffers) {
                String s = "" + StandardCharsets.UTF_8.decode(buffer);
                this.messageDataConsumer.accept(s);
            }
        } catch (Exception e) {
            this.future.completeExceptionally(e);
            this.subscription.cancel();
        }
        this.subscription.request(1);
    }

    @Override
    public void onError(Throwable throwable) {
        this.future.completeExceptionally(throwable);
    }

    @Override
    public void onComplete() {
        try {
            this.future.complete(null);
        } catch (Exception e) {
            this.future.completeExceptionally(e);
        }
    }
}
