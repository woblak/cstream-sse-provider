package com.woblak.cstream.provider.core.io.sse.controller;

import com.woblak.cstream.aggregator.api.event.MeanTradePrice;
import com.woblak.cstream.api.KafkaHeaders;
import com.woblak.cstream.api.KafkaTopics;
import com.woblak.cstream.provider.core.cache.AvailableCodes;
import com.woblak.cstream.provider.core.config.EmbeddedKafkaTest;
import com.woblak.cstream.provider.core.io.kafka.handler.AvailableCodeMsgHandler;
import com.woblak.cstream.provider.core.io.kafka.handler.MsgHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.annotation.DirtiesContext;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.awaitility.Awaitility.await;
import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Execution(ExecutionMode.SAME_THREAD)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
@Slf4j
class SseControllerTest extends EmbeddedKafkaTest {

    @LocalServerPort
    int serverPort;

    @Autowired
    AvailableCodes availableCodes;

    MsgHandler<MeanTradePrice> handler;

    BlockingQueue<ConsumerRecord<String, Object>> records;

    HttpClient httpClient;

    @BeforeEach
    void setUp() {
        this.httpClient = HttpClient.newHttpClient();
        this.handler = new AvailableCodeMsgHandler(availableCodes);
        this.records = new ArrayBlockingQueue<>(64);
        super.setTopics(KafkaTopics.CRYPTO_AGG_MEAN);
        super.addListener(record -> records.add(record));
        super.addMessageHandlerAsListener(handler, MeanTradePrice.class);
        super.start();
    }

    @AfterEach
    void tearDown() {
        super.stop();
    }

    @Test
    void shouldReturnAvailableCodes() throws IOException, InterruptedException {
        // given
        String code = "btcusd";
        Message<MeanTradePrice> msg = createMsg(code);

        // when
        String availableCodesBefore = getAvailableCodes();
        sendMeanTradePriceMsg(msg);
        String availableCodesAfter = getAvailableCodes();

        // then
        assertEquals("[]", availableCodesBefore);
        assertEquals("[\"" + code + "\"]", availableCodesAfter);
    }

    private String getAvailableCodes() throws IOException, InterruptedException {
        URI uri = URI.create("http://localhost:" + serverPort + "/events/codes");
        HttpRequest req = HttpRequest.newBuilder().GET().uri(uri).build();
        return httpClient.send(req, HttpResponse.BodyHandlers.ofString()).body();
    }

    private Message<MeanTradePrice> createMsg(String code) {
        var event = MeanTradePrice.builder()
                .code(code)
                .build();
        return MessageBuilder.withPayload(event)
                .setHeader(KafkaHeaders.TOPIC, KafkaTopics.CRYPTO_AGG_MEAN)
                .setHeader(KafkaHeaders.MESSAGE_KEY, code)
                .build();
    }

    private void sendMeanTradePriceMsg(Message<?> msg) {
        super.sendMessage(msg);
        await()
                .with()
                .alias("Wait until kafka messages will be received.")
                .given()
                .atMost(30, TimeUnit.SECONDS)
                .pollInterval(100, TimeUnit.MILLISECONDS)
                .then()
                .until(() -> records.size() > 0);
    }
}
