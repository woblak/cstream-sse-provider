package com.woblak.cstream.provider.core.io.sse.emitter;

import com.woblak.cstream.aggregator.api.event.SummaryTrade;
import com.woblak.cstream.api.KafkaHeaders;
import com.woblak.cstream.api.KafkaTopics;
import com.woblak.cstream.provider.core.config.EmbeddedKafkaTest;
import com.woblak.cstream.provider.core.config.SseBodySubscriber;
import com.woblak.cstream.provider.core.io.kafka.handler.SummaryMsgHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.annotation.DirtiesContext;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Execution(ExecutionMode.SAME_THREAD)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
@Slf4j
class CryptoSseEmitterTest extends EmbeddedKafkaTest {

    @LocalServerPort
    int serverPort;

    @Autowired
    SummaryMsgHandler summaryMsgHandler;

    BlockingQueue<ConsumerRecord<String, Object>> records;
    BlockingQueue<String> events;

    @BeforeEach
    void setUp() {
        this.records = new ArrayBlockingQueue<>(64);
        this.events = new ArrayBlockingQueue<>(64);
        super.setTopics(KafkaTopics.CRYPTO_AGG_SUMMARY);
        super.addListener(record -> records.add(record));
        super.addMessageHandlerAsListener(summaryMsgHandler, SummaryTrade.class);
        super.start();
    }

    @AfterEach
    void tearDown() {
        super.stop();
    }

    @Test
    void shouldEmitMsgOnlyWithWantedCode() {
        // given
        String wantedCode = "btcusd";
        String unwantedCode = "unwanted";

        // when
        connectToSse(wantedCode);

        // then
        testSseConnection();
        testReceivingKafkaMsgs(wantedCode, unwantedCode);
        testReceivingSseMsg(wantedCode, unwantedCode);
    }

    private Message<SummaryTrade> createMsg(String wantedCode, String windowName) {
        var wantedSummaryTrade = SummaryTrade.builder()
                .code(wantedCode)
                .windowDurationName(windowName)
                .metaData(SummaryTrade.MetaData.builder().endMs(91636205001152L).build())
                .data(null)
                .build();
        return MessageBuilder.withPayload(wantedSummaryTrade)
                .setHeader(KafkaHeaders.TOPIC, KafkaTopics.CRYPTO_AGG_SUMMARY)
                .setHeader(KafkaHeaders.MESSAGE_KEY, wantedCode)
                .build();
    }

    private void connectToSse(String wantedCode) {
        var uri = URI.create("http://localhost:" + serverPort + "/events/agg/" + wantedCode);
        var httpRequest = HttpRequest.newBuilder(uri).GET().build();
        var bodySubscriber = new SseBodySubscriber(i -> events.add(i));
        HttpClient.newHttpClient().sendAsync(httpRequest, resp -> bodySubscriber);
    }

    private void testSseConnection() {
        await()
                .with()
                .alias("Wait for first heartBeat on sse connections.")
                .given()
                .atMost(30, TimeUnit.SECONDS)
                .pollInterval(400, TimeUnit.MILLISECONDS)
                .then()
                .until(() -> events.size() > 0);
    }

    private void testReceivingKafkaMsgs(String wantedCode, String unwantedCode) {
        Message<SummaryTrade> msgWithWantedCode1 = createMsg(wantedCode, "PT1M");
        Message<SummaryTrade> msgWithWantedCode2 = createMsg(wantedCode, "PT10S");
        Message<SummaryTrade> msgWithUnwantedCode1 = createMsg(unwantedCode, "PT10S");
        super.sendMessage(msgWithWantedCode1);
        super.sendMessage(msgWithWantedCode2);
        super.sendMessage(msgWithUnwantedCode1);
        await()
                .with()
                .alias("Wait until kafka messages will be received.")
                .given()
                .atMost(2, TimeUnit.SECONDS)
                .pollInterval(100, TimeUnit.MILLISECONDS)
                .then()
                .until(() -> records.size() >= 2);
    }

    private void testReceivingSseMsg(String wantedCode, String unwantedCode) {
        Message<SummaryTrade> msgWithWantedCode1 = createMsg(wantedCode, "PT1M");
        Message<SummaryTrade> msgWithWantedCode2 = createMsg(wantedCode, "PT10S");
        Message<SummaryTrade> msgWithUnwantedCode1 = createMsg(unwantedCode, "PT10S");
        super.sendMessage(msgWithWantedCode1);
        super.sendMessage(msgWithWantedCode2);
        super.sendMessage(msgWithUnwantedCode1);

        await()
                .with()
                .alias("Wait until sse agg messages will be received.")
                .given()
                .atMost(2, TimeUnit.SECONDS)
                .pollInterval(100, TimeUnit.MILLISECONDS)
                .then()
                .until(() -> events.stream().filter(s -> s.matches(".*" + wantedCode + ".*")).count() >= 2);

        assertFalse(events.stream().anyMatch(s -> s.matches(".*" + unwantedCode + ".*")));
    }
}
